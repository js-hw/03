console.log("Task 1:")

let userNumber = prompt("Enter any integer number", "");

while (true) {
  if (userNumber % 1 !== 0) {
    userNumber = prompt("You've entered not enteger number", `${userNumber}`);
  }
  else {
    break;
  }
}

if (parseInt(userNumber) > 4) {
  for (let i = 0; i <= userNumber; i++) {
    if (i % 5 === 0) {
      console.log(i);
    }
  }
}
else if (parseInt(userNumber) < -4) {
  for (let i = 0; i >= userNumber; i--) {
    if (i % 5 === 0) {
      console.log(i);
    }
  }
} 
else {
  console.dir("Sorry, no numbers");
}

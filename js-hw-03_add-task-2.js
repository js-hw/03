console.log("")
console.log("Task 2:")

let m = +prompt("Enter a lower value (m)","");
let n = +prompt("Enter a higher value (n)","");

while (true) {
    if (m >= n) {
        alert("Unfortunately, the condition is not fulfilled! The first value (m) must be strictly less the second (n)");
        m = prompt("Enter a lower value (m)","");
        n = prompt("Enter a higher value (n)","");
    }
    else {
        break;
    }
}

for (let i = +m; i <= +n; i++) {
    if (i / i == 1 && i % 1 == 0) {
        console.log(i);
    }
}

// Нажаль, перевірку на прості числа виконати не зміг
// Буду вдячний за підказку!